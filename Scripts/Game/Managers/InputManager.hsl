enum {
    Input_UP,
    Input_DOWN,
    Input_LEFT,
    Input_RIGHT,
    Input_JUMP,
    Input_ATTACK,
    Input_START,
    Input_SELECT,
    Input_MAX
}

enum {
    MetaKey_FULLSCREEN,
    MetaKey_DEVMENU,
    MetaKey_MAX
}

var Button_TRIGGERLEFT   = NUM_CONTROLLER_BUTTONS;
var Button_TRIGGERRIGHT  = NUM_CONTROLLER_BUTTONS + 1;
var NumControllerButtons = NUM_CONTROLLER_BUTTONS + 2;

var ButtonNames = Array.Create(NumControllerButtons);
var ButtonNameMap = {};

var InputList = [ "up", "down", "left", "right", "jump", "attack", "start", "select" ];
var MetaKeyList = [ "fullscreen", "devMenu" ];

var Input_PlayerCount = 4;

var User_Keys = Array.Create(Input_PlayerCount);
var User_Controllers = Array.Create(Input_PlayerCount);
var User_ControllerIDs = Array.Create(Input_PlayerCount);
var User_Defined = Array.Create(Input_PlayerCount);
var User_KeyIDs = Array.Create(Input_PlayerCount);
var User_Buttons = Array.Create(Input_PlayerCount);
var User_Rumble = Array.Create(Input_PlayerCount);
var User_Deadzones = Array.Create(Input_PlayerCount);

var MetaKeys = Array.Create(2);
for (var i = 0; i < 2; i++)
    MetaKeys[i] = {};

var Device_None = -1;
var Device_Keyboard = 0;
var Device_Controller = 1;

var DeviceNames = [
    "keyboard",
    "controller"
];
var DeviceFields = [
    [ ],
    [ "index", "deadzone", "rumble" ]
];

var FieldPrefixes = [];
for (var i = 0; i < 2; i++) {
    var result;
    if (i != Device_Keyboard)
        result = DeviceNames[i] + "_";
    else
        result = "";
    Array.Push(FieldPrefixes, result);
}

var SettingsFields = Array.Create(2);
var MetaKeysForDevice = Array.Create(2);
var PlayerInputFields = Array.Create(0);
var MetaKeyFields = Array.Create(0);

var MetaKeysSection = "keys";

var InputManager_Loaded = false;

class InputManager {
    event Create() {
        this.InputList = InputList;
        this.MetaKeyList = MetaKeyList;
        this.NumInputs = Input_MAX;
        this.NumPlayers = Input_PlayerCount;
        this.PlayersInGame = 4;

        this.ListenDevice = Array.Create(this.NumPlayers, Device_None);

        this.Held = Array.Create(this.NumPlayers, 0);
        this.Pressed = Array.Create(this.NumPlayers, 0);
        this.AnyHeld = Array.Create(this.NumPlayers, 0);
        this.AnyPressed = Array.Create(this.NumPlayers, 0);

        // Mirrors of this.Held[0] and this.Pressed[0]
        this.Up =
        this.Down =
        this.Left =
        this.Right =
        this.Jump =
        this.Attack =
        this.Start =
        this.Select = false;

        this.UpPressed =
        this.DownPressed =
        this.LeftPressed =
        this.RightPressed =
        this.JumpPressed =
        this.AttackPressed =
        this.StartPressed =
        this.SelectPressed = false;

        this.Pauseable = false;

        this.outKeys = Array.Create(this.NumInputs * 2);
        this.outJoys = Array.Create(this.NumInputs * 2);
        this.FirstFrame = true;

        this.User_Keys = User_Keys;
        this.User_Controllers = User_Controllers;
        this.User_ControllerIDs = User_ControllerIDs;
        this.User_Defined = User_Defined;
        this.User_KeyIDs = User_KeyIDs;
        this.User_Buttons = User_Buttons;
        this.User_Rumble = User_Rumble;
        this.User_Deadzones = User_Deadzones;

        this.Triggers = [ Axis_TRIGGERLEFT, Axis_TRIGGERRIGHT ];

        if (!InputManager_Loaded) {
            this.Initialize();
            this.Persistence = Persistence_GAME;
            InputManager_Loaded = true;
        }
    }

    event GetHeld(button, id) {
        return (this.Held[id] & (1 << button)) != 0;
    }
    event GetPressed(button, id) {
        return (this.Pressed[id] & (1 << button)) != 0;
    }

    event CleanSOCD(a, b, id) {
        var ab = (1 << a) | (1 << b);
        if ((this.Held[id] & ab) == ab)
            this.Held[id] &= ~ab;
        if ((this.Pressed[id] & ab) == ab)
            this.Pressed[id] &= ~ab;
    }

    event UpdateEarly() {
        for (var i = 0; i < this.PlayersInGame; i++) {
            this.UpdateController(i);
            this.GetInput(i);
        }

        this.Up             = this.GetHeld(Input_UP, 0);
        this.Down           = this.GetHeld(Input_DOWN, 0);
        this.Left           = this.GetHeld(Input_LEFT, 0);
        this.Right          = this.GetHeld(Input_RIGHT, 0);
        this.Jump           = this.GetHeld(Input_JUMP, 0);
        this.Attack         = this.GetHeld(Input_ATTACK, 0);
        this.Start          = this.GetHeld(Input_START, 0);
        this.Select         = this.GetHeld(Input_SELECT, 0);

        this.UpPressed      = this.GetPressed(Input_UP, 0);
        this.DownPressed    = this.GetPressed(Input_DOWN, 0);
        this.LeftPressed    = this.GetPressed(Input_LEFT, 0);
        this.RightPressed   = this.GetPressed(Input_RIGHT, 0);
        this.JumpPressed    = this.GetPressed(Input_JUMP, 0);
        this.AttackPressed  = this.GetPressed(Input_ATTACK, 0);
        this.StartPressed   = this.GetPressed(Input_START, 0);
        this.SelectPressed  = this.GetPressed(Input_SELECT, 0);

        this.FirstFrame = false;
    }

    event Update() {
        if (this.IsMetaKeyPressedForDevice(MetaKey_FULLSCREEN, Device_Controller))
            WindowManager.ToggleFullscreen();
    }

    event CheckListenable(listen) {
        if (listen == null)
            return false;

        var numInputs = this.NumInputs;
        for (var i = 0; i < numInputs; i++) {
            if (listen[i])
                return true;
        }
    }
    event GetInput(id) {
        var listenTo = null;
        var listenDevice = Device_None;

        // Controller input takes priority
        var joy = this.GetInputFromController(id);
        if (this.CheckListenable(joy)) {
            listenTo = joy;
            listenDevice = Device_Controller;
        }
        else {
            var key = this.GetInputFromKeyboard(id);
            if (this.CheckListenable(key)) {
                listenTo = key;
                listenDevice = Device_Keyboard;
            }
        }

        if (listenTo == null) {
            this.Held[id] = this.Pressed[id] = this.AnyHeld[id] = this.AnyPressed[id] = 0;
            return;
        }

        var curListenDevice = this.ListenDevice[id];
        if (curListenDevice != listenDevice) {
            if (curListenDevice == Device_Controller && listenDevice != Device_Controller)
                this.StopControllerRumble(this.User_ControllerIDs[id]);
            this.ListenDevice[id] = listenDevice;
        }

        var bit = 1;
        for (var btn = 0; btn < Input_MAX; btn++) {
            // Held
            if (listenTo[btn])
                this.Held[id] |= bit;
            else
                this.Held[id] &= ~bit;

            // Pressed
            if (listenTo[btn + Input_MAX])
                this.Pressed[id] |= bit;
            else
                this.Pressed[id] &= ~bit;

            bit <<= 1;
        }

        // SOCD cleaning
        this.CleanSOCD(Input_UP, Input_DOWN, id);
        this.CleanSOCD(Input_LEFT, Input_RIGHT, id);

        this.AnyHeld[id] = this.AnyPressed[id] = false;

        for (var btn = 0; btn < Input_MAX; btn++) {
            this.AnyHeld[id] |= this.GetHeld(btn, id);
            this.AnyPressed[id] |= this.GetPressed(btn, id);
        }
    }

    event GetInputFromKeyboard(index) {
        if (!this.GetDeviceDefined(Device_Keyboard, index))
            return null;

        var out = this.outKeys;
        var list = this.User_KeyIDs[index];
        var num = this.NumInputs;
        for (var i = 0; i < num; i++) {
            var key = list[i];
            if (key != null) {
                out[i] = Input.IsKeyDown(key);
                out[i + num] = Input.IsKeyPressed(key);
            } else {
                out[i] = false;
                out[i + num] = false;
            }
        }

        return out;
    }
    event GetInputFromController(index) {
        if (!this.GetDeviceDefined(Device_Controller, index))
            return null;

        var controller_index = this.User_ControllerIDs[index];
        if (controller_index < 0 || !Controller.IsConnected(controller_index))
            return null;

        var out = this.outJoys;
        var buttons = this.User_Buttons[index];
        var deadzone = this.User_Deadzones[index];

        // Stick controls
        out[Input_UP]    = Controller.GetAxis(controller_index, Axis_LEFTY) < -deadzone;
        out[Input_DOWN]  = Controller.GetAxis(controller_index, Axis_LEFTY) >  deadzone;
        out[Input_LEFT]  = Controller.GetAxis(controller_index, Axis_LEFTX) < -deadzone;
        out[Input_RIGHT] = Controller.GetAxis(controller_index, Axis_LEFTX) >  deadzone;

        // D-Pad controls
        out[Input_UP]    |= this.IsButtonDown(controller_index, buttons[Input_UP]);
        out[Input_DOWN]  |= this.IsButtonDown(controller_index, buttons[Input_DOWN]);
        out[Input_LEFT]  |= this.IsButtonDown(controller_index, buttons[Input_LEFT]);
        out[Input_RIGHT] |= this.IsButtonDown(controller_index, buttons[Input_RIGHT]);

        // Face buttons
        for (var btn = Input_JUMP; btn < Input_MAX; btn++)
            out[btn] = this.IsButtonDown(controller_index, buttons[btn]);

        // Determine "pressed" state for buttons
        if (!this.FirstFrame) {
            for (var btn = 0; btn < Input_MAX; btn++)
                out[btn + Input_MAX] = this.IsButtonPressed(controller_index, buttons[btn]);
        }

        return out;
    }

    event UpdateController(controller_index) {
        if (!Controller.IsConnected(controller_index))
            return;

        var pressed = global.ControllerPressed[controller_index];
        var held = global.ControllerHeld[controller_index];
        var triggers = this.Triggers;
        var btn = 0;

        for (var i = 0; i < NUM_CONTROLLER_BUTTONS; i++) {
            pressed[btn] = Controller.IsButtonPressed(controller_index, i);
            held[btn] = Controller.IsButtonHeld(controller_index, i);
            btn++;
        }

        for (var i = 0; i < 2; i++) {
            var isDown = Controller.GetAxis(controller_index, triggers[i]) > 0.3; // TODO
            pressed[btn] = !held[btn] && isDown;
            held[btn] = isDown;
            btn++;
        }
    }
    event IsButtonDown(index, button) {
        if (button == null)
            return false;
        return global.ControllerHeld[index][button];
    }
    event IsButtonPressed(index, button) {
        if (button == null)
            return false;
        return global.ControllerPressed[index][button];
    }
    event GetControllerHeld(index) {
        if (Controller.IsConnected(index))
            return global.ControllerHeld[index];
    }
    event GetControllerPressed(index) {
        if (Controller.IsConnected(index))
            return global.ControllerPressed[index];
    }
    event GetControllerIndex(playerID) {
        return User_ControllerIDs[playerID];
    }
    event GetButtonName(button) {
        return ButtonNames[button];
    }

    event RumbleController(controllerID, frequency, duration) {
        if (controllerID < 0 || !Controller.IsConnected(controllerID))
            return;

        if (frequency <= 0.0)
            return;

        Controller.Rumble(controllerID, Math.Min(frequency, 1.0), duration);
    }
    event StopControllerRumble(controllerID) {
        if (controllerID < 0 || !Controller.IsConnected(controllerID))
            return;

        Controller.StopRumble(controllerID);
    }

    event CheckCanRumble(playerID) {
        return this.User_Rumble[playerID] && this.ListenDevice[playerID] == Device_Controller;
    }

    event Rumble(playerID, frequency, duration) {
        if (this.CheckCanRumble(playerID))
            this.RumbleController(this.User_ControllerIDs[playerID], frequency, duration);
    }
    event StopRumble(playerID) {
        if (this.CheckCanRumble(playerID))
            this.StopControllerRumble(this.User_ControllerIDs[playerID]);
    }

    event SetRumblePaused(paused) {
        var count = Controller.GetCount();
        for (var i = 0; i < count; i++) {
            if (Controller.IsConnected(i))
                Controller.SetRumblePaused(i, paused);
        }
    }
    event StopAllRumble() {
        var count = Controller.GetCount();
        for (var i = 0; i < count; i++) {
            if (Controller.IsConnected(i) && Controller.IsRumbleActive(i))
                Controller.StopRumble(i);
        }
    }

    event MakeFieldsForDevice(device, source, destFieldsForPlayer) {
        var length = Array.Length(source);
        var dest = Array.Create(length);

        for (var i = 0; i < length; i++) {
            dest[i] = this.GetCombinedInput(device, source[i]);
            Array.Push(destFieldsForPlayer, dest[i]);
        }

        return dest;
    }

    event Initialize() {
        global.ControllerHeld = Array.Create(this.NumPlayers);
        global.ControllerPressed = Array.Create(this.NumPlayers);

        for (var i = 0; i < this.NumPlayers; i++) {
            global.ControllerHeld[i] = Array.Create(NumControllerButtons);
            global.ControllerPressed[i] = Array.Create(NumControllerButtons);
        }

        DeviceFields[0] = this.InputList;
        for (var i = 0; i < Array.Length(this.InputList); i++)
            Array.Push(DeviceFields[1], this.InputList[i]);

        for (var devID = 0; devID < 2; devID++) {
            SettingsFields[devID] = this.MakeFieldsForDevice(devID, DeviceFields[devID], PlayerInputFields);
            MetaKeysForDevice[devID] = this.MakeFieldsForDevice(devID, MetaKeyList, MetaKeyFields);
        }

        for (var i = 0; i < Input_PlayerCount; i++) {
            this.User_Keys[i] = {};
            this.User_Controllers[i] = {};
            this.User_KeyIDs[i] = Array.Create(Input_MAX);
            this.User_Buttons[i] = Array.Create(Input_MAX);
            this.User_Defined[i] = 0;
        }

        for (var i = 0; i < NUM_CONTROLLER_BUTTONS; i++)
            this.InitButtonName(Input.GetButtonName(i), i);
        this.InitButtonName(Input.GetAxisName(Axis_TRIGGERLEFT), Button_TRIGGERLEFT);
        this.InitButtonName(Input.GetAxisName(Axis_TRIGGERRIGHT), Button_TRIGGERRIGHT);

        for (var i = 0; i < this.NumPlayers; i++)
            this.LoadPlayerInputs(i);

        this.LoadMetaKeys();
    }

    event InitButtonName(name, button) {
        ButtonNames[button] = name;
        ButtonNameMap[name] = button;
    }

    event GetDeviceControls(device) {
        if (device == Device_Keyboard)
            return User_Keys;
        else if (device == Device_Controller)
            return User_Controllers;
    }

    event GetDeviceButtons(device) {
        if (device == Device_Keyboard)
            return User_KeyIDs;
        else if (device == Device_Controller)
            return User_Buttons;
    }

    event GetMetaKeys(device) {
        return MetaKeys[device];
    }

    // Loads the default controls from the GameConfig
    event LoadDefaultControls(id, dest) {
        var gameConfig = global.GameConfig;
        if (gameConfig == null)
            return;

        var defaults = gameConfig["controls"]["player"];
        if (typeof defaults == "map") {
            if (id == 0)
                this.CopyDefaults(dest, defaults);
        }
        else if (typeof defaults == "array") {
            var source = this.FindDefaultControlsForPlayer(defaults, id + 1);
            if (source == null)
                return;

            this.CopyDefaults(dest, source);
        }
    }

    // Gets the settings section for the specified player ID
    event GetSettingsSection(id) {
        return "player" + (id + 1);
    }

    // Initializes the settings
    event InitPlayerSettings(id, controls) {
        this.InitSettings(this.GetSettingsSection(id), PlayerInputFields, controls);
    }

    // Read controls from the settings
    event ReadSettingsControls(id, controls) {
        this.ReadSettingsSection(this.GetSettingsSection(id), PlayerInputFields, controls);
    }

    // Sets the controls for the specified player
    event SetControlsForPlayer(id, source, device) {
        var dest = this.GetDeviceControls(device)[id];
        var fields = SettingsFields[device];
        dest["defined"] = false;
        this.ParseSettings(dest, fields, source, device);
    }

    event LoadPlayerInputs(id) {
        var controls = {};

        this.LoadDefaultControls(id, controls);
        this.InitPlayerSettings(id, controls);
        this.ReadSettingsControls(id, controls);

        for (var dev = 0; dev < 2; dev++)
            this.SetControlsForPlayer(id, controls, dev);

        var controller_info = this.User_Controllers[id];
        if (controller_info["index"] == null)
            controller_info["index"] = id;
        else
            controller_info["index"]--;
        if (controller_info["deadzone"] == null)
            controller_info["deadzone"] = 0.5;

        for (var dev = 0; dev < 2; dev++)
            this.SetupDevice(dev, id);
    }

    event CopyButtons(src, dest) {
        dest[Input_UP]      = src["up"];
        dest[Input_DOWN]    = src["down"];
        dest[Input_LEFT]    = src["left"];
        dest[Input_RIGHT]   = src["right"];
        dest[Input_JUMP]    = src["jump"];
        dest[Input_ATTACK]  = src["attack"];
        dest[Input_START]   = src["start"];
        dest[Input_SELECT]  = src["select"];
    }

    event SetupDevice(device, id) {
        var src, dest;

        if (device == Device_Keyboard) {
            src = User_Keys[id];
            dest = User_KeyIDs[id];
        }
        else if (device == Device_Controller) {
            src = User_Controllers[id];
            dest = User_Buttons[id];
            User_ControllerIDs[id] = src["index"];
            User_Deadzones[id] = src["deadzone"];
            User_Rumble[id] = src["rumble"];
        }

        InputManager.CopyButtons(src, dest);
        InputManager.SetDeviceDefined(device, id, src["defined"]);
    }

    event SetDeviceDefined(device, id, defined) {
        var flag = 1 << device;
        if (defined)
            User_Defined[id] |= flag;
        else
            User_Defined[id] &= ~flag;
    }
    event GetDeviceDefined(device, id) {
        return User_Defined[id] & (1 << device);
    }

    event IsMetaKeyDebug(metaKey) {
        return metaKey == MetaKey_DEVMENU;
    }

    // Loads the default meta keys from the GameConfig
    event LoadDefaultMetaKeys(dest) {
        var gameConfig = global.GameConfig;
        if (gameConfig == null)
            return;

        var defaults = gameConfig["keys"];
        var keyList = MetaKeyList;
        for (var i = 0; i < Array.Length(MetaKeyList); i++)
            dest[keyList[i]] = defaults[keyList[i]];
    }

    // Initializes the settings
    event InitMetaKeySettings(keys) {
        if (!Settings.SectionExists(MetaKeysSection))
            Settings.AddSection(MetaKeysSection);

        var fields = MetaKeyFields;
        for (var i = 0; i < Array.Length(fields); i++) {
            var key = fields[i];
            var value = keys[key];
            if (value) {
                if (this.IsMetaKeyDebug(value) && !global.Debug)
                    continue;
                if (!Settings.PropertyExists(MetaKeysSection, key))
                    Settings.SetString(MetaKeysSection, key, value);
            }
        }
    }

    // Read meta keys from the settings
    event ReadMetaKeys(keys) {
        var section = MetaKeysSection;
        var fields = MetaKeyFields;
        for (var i = 0; i < Array.Length(fields); i++)
            keys[fields[i]] = Settings.GetString(MetaKeysSection, fields[i]);
    }

    event LoadMetaKeys() {
        var keys = {};

        this.LoadDefaultMetaKeys(keys);
        this.InitMetaKeySettings(keys);
        this.ReadMetaKeys(keys);

        for (var dev = 0; dev < 2; dev++)
            this.ParseMetaKeys(keys, dev);

        var metaKeys = MetaKeys[Device_Keyboard];
        if (metaKeys["defined"]) {
            var fullscreenKey = metaKeys["fullscreen"];
            if (fullscreenKey)
                Application.SetKeyBind(KeyBind_Fullscreen, fullscreenKey);
        }
    }

    event ParseMetaKeys(source, device) {
        this.ParseSettings(MetaKeys[device], MetaKeysForDevice[device], source, device);
    }

    event FindDefaultControlsForPlayer(content, id) {
        for (var i = 0; i < Array.Length(content); i++) {
            var c = content[i];
            if (String.ParseDecimal(c["#id"]) == id)
                return c;
        }
    }

    event CopyDefaults(dest, source) {
        for (var device = 0; device < 2; device++) {
            var content = source[DeviceNames[device]];
            if (content == null)
                continue;

            var srcFields = DeviceFields[device];
            var destFields = SettingsFields[device];
            for (var i = 0; i < Array.Length(srcFields); i++)
                dest[destFields[i]] = content[srcFields[i]];
        }
    }

    event InitSettings(section, fields, controls) {
        if (!Settings.SectionExists(section))
            Settings.AddSection(section);

        for (var i = 0; i < Array.Length(fields); i++) {
            var key = fields[i];
            var value = controls[key];
            if (!Settings.PropertyExists(section, key) && value)
                Settings.SetString(section, key, value);
        }
    }

    event GetCombinedInput(device, field) {
        return FieldPrefixes[device] + field;
    }

    event ExtractCombinedInput(device, field) {
        var prefix = FieldPrefixes[device];
        var length = String.Length(prefix);
        if (length != 0) {
            var ind = String.IndexOf(field, FieldPrefixes[device]);
            var start;
            if (ind > -1) {
                start = ind + length;
                field = String.Substring(field, start, String.Length(field) - start);
            }
        }
        return field;
    }

    event ReadSettingsSection(section, fields, controls) {
        for (var i = 0; i < Array.Length(fields); i++)
            controls[fields[i]] = Settings.GetString(section, fields[i]);
    }

    event ParseSettings(dest, fields, source, device) {
        var isDefined = false;

        for (var i = 0; i < Array.Length(fields); i++) {
            var key = fields[i];
            var value = source[key];
            if (value == null)
                continue;

            var parsed;
            if (device == Device_Controller) {
                key = this.ExtractCombinedInput(device, key);
                parsed = this.ParseControllerField(key, value);
            }
            else
                parsed = this.ParseKeyString(value);

            if (parsed != null) {
                dest[key] = parsed;
                isDefined = true;
            }
        }

        if (isDefined)
            dest["defined"] = true;
    }

    event SetSetting(device, dest, key, value) {
        if (device == Device_Controller)
            key = InputManager.ExtractCombinedInput(device, key);

        if (value != null) {
            if (device == Device_Controller)
                dest[key] = InputManager.ParseControllerField(key, value);
            else
                dest[key] = InputManager.ParseKeyString(value);
        }
        else
            dest[key] = null;

        return dest[key];
    }

    event SetControl(id, device, button, value) {
        var dest = InputManager.GetDeviceControls(device)[id];
        var key = InputList[button];

        var parsed = InputManager.SetSetting(device, dest, key, value);

        InputManager.SetupDevice(device, id);

        // Save to settings
        var section = this.GetSettingsSection(id);
        key = InputManager.GetCombinedInput(device, key);

        if (parsed != null)
            Settings.SetString(section, key, value);
        else
            Settings.RemoveProperty(section, key);
    }

    event SetMetaKey(device, metaKey, value) {
        var dest = MetaKeys[device];
        var key = MetaKeyList[metaKey];

        var parsed = InputManager.SetSetting(device, dest, key, value);
        if (device == Device_Keyboard && parsed != null && metaKey == MetaKey_FULLSCREEN)
            Application.SetKeyBind(KeyBind_Fullscreen, parsed);

        // Save to settings
        key = InputManager.GetCombinedInput(device, key);

        if (InputManager.IsMetaKeyDebug(metaKey) && !global.Debug) {
            if (!Settings.PropertyExists(MetaKeysSection, key))
                return;
        }

        if (parsed != null)
            Settings.SetString(MetaKeysSection, key, value);
        else
            Settings.RemoveProperty(MetaKeysSection, key);
    }

    event ParseKeyString(name) {
        var g = Input.ParseKeyName(name);
        if (g != null)
            return g;
        return String.ParseInteger(name);
    }

    event ParseControllerField(key, value) {
        if (key == "rumble")
            return InputManager.ParseBoolean(value);
        else if (key == "deadzone")
            return String.ParseDecimal(value);
        else if (key == "index")
            return String.ParseInteger(value);
        else
            return ButtonNameMap[value];
    }

    event ParseBoolean(string) {
        if (string == "true" || string == "1")
            return true;
        else
            return false;
    }

    event IsHeldForDevice(which, device, controller_index) {
        if (device == Device_Keyboard)
            return Input.IsKeyDown(which);
        else if (device == Device_Controller)
            return global.ControllerHeld[controller_index][which];
        else
            return false;
    }
    event IsPressedForDevice(which, device, controller_index) {
        if (device == Device_Keyboard)
            return Input.IsKeyPressed(which);
        else if (device == Device_Controller)
            return global.ControllerPressed[controller_index][which];
        else
            return false;
    }

    event CheckMetaKeyForDevice(metaKey, device, held) {
        var metaKeys = MetaKeys[device];
        if (!metaKeys["defined"])
            return false;

        var which = metaKeys[MetaKeyList[metaKey]];
        if (!which)
            return;

        if (held)
            return InputManager.IsHeldForDevice(which, device, 0);
        return InputManager.IsPressedForDevice(which, device, 0);
    }
    event CheckMetaKey(metaKey, held) {
        for (var i = 0; i < 2; i++) {
            if (InputManager.CheckMetaKeyForDevice(metaKey, i, held))
                return true;
        }
        return false;
    }

    event IsMetaKeyHeldForDevice(metaKey, device) {
        return InputManager.CheckMetaKeyForDevice(metaKey, device, true);
    }
    event IsMetaKeyHeld(metaKey) {
        return InputManager.CheckMetaKey(metaKey, true);
    }

    event IsMetaKeyPressedForDevice(metaKey, device) {
        return InputManager.CheckMetaKeyForDevice(metaKey, device, false);
    }
    event IsMetaKeyPressed(metaKey) {
        return InputManager.CheckMetaKey(metaKey, false);
    }
}
